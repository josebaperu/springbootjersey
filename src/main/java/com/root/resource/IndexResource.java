package com.root.resource;

import org.glassfish.jersey.process.internal.RequestScoped;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/say")
@RequestScoped
public class IndexResource {

    @GET
    @Path("/yay")
    @Produces(MediaType.TEXT_PLAIN)
    public String say(){
        return "yayyy";
    }
}
